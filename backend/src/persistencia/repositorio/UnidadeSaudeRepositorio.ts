import { unidadeSaudeModel } from "../model/unidadeSaudeModel";
import { unidadeSaude } from "../../entidades/unidadeSaude";
import { AtendimentoRepositorio } from "./AtendimentoRepositorio";

export class UnidadeSaudeRepositorio{
    static async getUnidades():Promise<unidadeSaude[]>{
        return unidadeSaudeModel.find().exec();
    }

    static async getUnidadeById(id:string):Promise<unidadeSaude|null>{
        return unidadeSaudeModel.findById(id);
    }

    static async inserirUnidade(unidade:unidadeSaude):Promise<unidadeSaude>{
        return unidadeSaudeModel.create(unidade);
    }

    static async atualizarUnidade(id:string, unid:unidadeSaude):Promise<unidadeSaude>{
        const unidade = await unidadeSaudeModel.findById(id);
        if(unidade){
            unidade.idUnidade = unid.idUnidade;
            unidade.nome = unid.nome;
            unidade.email = unid.email;
            unidade.endereco = unid.endereco;
            return unidade.save();
        }else{
            throw new Error('id inexistente');
        }
    }

    static async removerUnidade(id:string):Promise<unidadeSaude>{
        const unidade = await unidadeSaudeModel.findById(id);
        if(unidade){
            return unidade.remove();
        }else{
            throw new Error('id inexistente');
        }
    }

    static async getTemposUnidade(idUnid:number):Promise<Object>{
        const atendimentos = await AtendimentoRepositorio.getAtendimentosByUnidade(idUnid);
        if(atendimentos.length>0){
            let maiorTempo = atendimentos[0].duracao;
            let menorTempo = atendimentos[0].duracao;
            let somaTempos = 0;
            atendimentos.forEach(atendimento=>{
                if(atendimento.duracao > maiorTempo){
                    maiorTempo = atendimento.duracao;
                }
                if(atendimento.duracao < menorTempo){
                    menorTempo = atendimento.duracao;
                }
                somaTempos += atendimento.duracao;
            });
            return{
                maiorTempo:maiorTempo,
                menorTempo:menorTempo,
                mediaTempo:somaTempos/atendimentos.length
            }

        }else{
            throw new Error('unidade sem atendimentos')
        }
    }
}