import { pacienteModel } from "../model/pacienteModel";
import { paciente } from "../../entidades/paciente";

export class PacienteRepositorio{
    static async getPacientes():Promise<paciente[]>{
        return pacienteModel.find().exec();
    }

    static async getPacienteById(id:string):Promise<paciente|null>{
        return pacienteModel.findById(id);
    }

    static async getPacienteByCpf(cpf:string):Promise<paciente|null>{
        return pacienteModel.findOne().where("cpf").equals(cpf).exec();
    }

    static async inserirPaciente(paciente:paciente):Promise<paciente>{
        return pacienteModel.create(paciente)
    }

    static async atualizarPaciente(id:string, pac:paciente):Promise<paciente>{
        const paciente = await pacienteModel.findById(id);
        if(paciente){
            paciente.nome = pac.nome;
            paciente.cpf = pac.cpf;
            paciente.numContato = pac.numContato;
            paciente.idade = pac.idade;
            paciente.sexo = pac.sexo;
            paciente.peso = pac.peso;
            paciente.altura = pac.altura;
            paciente.endereco = pac.endereco;

            return paciente.save();
        }else{
            throw new Error('id inexistente');
        }
    }

    static async removerPaciente(id:string):Promise<paciente>{
        const paciente = await pacienteModel.findById(id);
        if(paciente){
            return paciente.remove();
        }else{
            throw new Error('id inexistente');
        }
    }
}