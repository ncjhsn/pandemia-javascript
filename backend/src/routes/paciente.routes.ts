import { Router } from 'express';
import * as PacienteController from '../api/controladores/paciente/paciente.controller';

export const routerPaciente = Router();
export const path = '/paciente'; 

routerPaciente.get(`${path}/busca`, PacienteController.getpacientes);
routerPaciente.get(`${path}/busca-cpf/:cpf`, PacienteController.getPacienteByCpf);
routerPaciente.get(`${path}/busca-id/:id`, PacienteController.getpacienteById);
routerPaciente.post(`${path}/cadastrar`, PacienteController.inserirPaciente);
routerPaciente.delete(`${path}/remover/:id`, PacienteController.deletarPaciente);
routerPaciente.patch(`${path}/atualizar/:id`, PacienteController.atualizarPaciente);