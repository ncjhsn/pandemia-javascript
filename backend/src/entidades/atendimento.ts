import { paciente } from "./paciente";

export interface atendimento {
    idUnidade: number,
    paciente: paciente,
    idAtendimento: string,
    dataAtendimento: Date,
    duracao: number,
    possibContagio: boolean,
    teste1?: boolean,
    teste2?: boolean
}
