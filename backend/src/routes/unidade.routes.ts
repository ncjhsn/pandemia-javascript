import { Router } from 'express';
import * as UnidadeController from '../api/controladores/unidadeSaude/unidadeSaude.controller';

export const routerUnidade = Router(); 
export const path = '/unidade';

routerUnidade.get(`${path}/busca`, UnidadeController.getUnidades);
routerUnidade.get(`${path}/busca-id/:id`, UnidadeController.getunidadeSaudeById);
routerUnidade.delete(`${path}/remover/:id`, UnidadeController.removerUnidade);
routerUnidade.post(`${path}/cadastrar`, UnidadeController.inserirUnidade);
routerUnidade.patch(`${path}/atualizar`, UnidadeController.atualizarUnidade);
routerUnidade.get(`${path}/busca-tempo`, UnidadeController.getTemposUnidade);

