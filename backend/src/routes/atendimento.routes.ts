import { Router } from 'express';
import * as AtendimentoController from '../api/controladores/atendimento/atendimento.controller';

export const routerAtendimento = Router();
export const path = '/atendimento'; 

routerAtendimento.get(`${path}/busca`, AtendimentoController.getAtendimentos);
routerAtendimento.get(`${path}/busca-data/:date`, AtendimentoController.getAtendimentosByData);
routerAtendimento.get(`${path}/busca-unidade/:unidade`, AtendimentoController.getAtendimentosByUnidade);
routerAtendimento.get(`${path}/busca-id/:id`, AtendimentoController.getAtendimentoById);
routerAtendimento.post(`${path}/cadastrar`, AtendimentoController.inserirAtendimento);
routerAtendimento.patch(`${path}/atualizar/:id`, AtendimentoController.atualizarAtendimento);
routerAtendimento.delete(`${path}/remover/:id`, AtendimentoController.deletarAtendimento);

