import app from './app';
import { connect } from 'mongoose';
import { PacienteRepositorio } from './persistencia/repositorio/PacienteRepositorio';
import { UnidadeSaudeRepositorio } from './persistencia/repositorio/UnidadeSaudeRepositorio';
import { AtendimentoRepositorio } from './persistencia/repositorio/AtendimentoRepositorio';

(async () => {
    try {
        const url = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`;
        const cliente = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('Conectado com sucesso ao MongoDB');
        
        let u1 = await UnidadeSaudeRepositorio.getUnidades();
        console.log(u1);

        app.listen(app.get('port'), () => {
            console.log(`Express na porta ${app.get('port')}`);
        });
    } catch (e) {
        console.log(e);
        console.log(e.message);
    }
})();