import { Request, Response, NextFunction } from 'express';
import { atendimento } from '../../../entidades/atendimento';
import { AtendimentoRepositorio } from '../../../persistencia/repositorio/AtendimentoRepositorio';


export async function inserirAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        console.log(req.body)
        const atendimento = req.body as atendimento;
        await AtendimentoRepositorio.inserirAtendimento(atendimento);
        res.status(201).send('Cadastro realizado');
    } catch (e) {
        next(e);
    }
}

export async function getAtendimentos(req: Request, res: Response, next: NextFunction) {
    try {
        const atendimentos = await AtendimentoRepositorio.getAtendimentos();
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }
}

export async function deletarAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        const a = req.body.id;
        const atendimentos = await AtendimentoRepositorio.removerAtendimento(a);
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }
}

export async function getAtendimentoById(req: Request, res: Response, next: NextFunction) {
    try {
        const idAtendimento = req.params.idAtendimento;
        const atendimento = await AtendimentoRepositorio.getAtendimentoById(idAtendimento);
        res.json(atendimento);
    } catch (e) {
        next(e);
    }
}
export async function getAtendimentosByUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const idUnidade = req.body.idUnidade;
        const atendimento = await AtendimentoRepositorio.getAtendimentosByUnidade(idUnidade);
        res.json(atendimento);
    } catch (e) {
        next(e);
    }
}

export async function getAtendimentosByData(req: Request, res: Response, next: NextFunction) {
    try {
        const date = req.body.date;
        const atendimento = await AtendimentoRepositorio.getAtendimentosByData(date);
        res.json(atendimento);
    } catch (e) {
        next(e);
    }
}

export async function atualizarAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.body.id;
        const atendimento = req.body.atendimento;
        const atendimentoAtt = await AtendimentoRepositorio.atualizarAtendimento(id, atendimento);
        res.json(atendimentoAtt);
    } catch (e) {
        next(e);
    }
}





