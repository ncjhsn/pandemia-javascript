export interface unidadeSaude {
    idUnidade?: number,
    nome: string,
    email: string,
    endereco?: {
        rua: string,
        numero: number,
        bairro: string,
        cidade: string
    }
}
