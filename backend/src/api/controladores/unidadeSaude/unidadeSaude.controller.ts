import { Request, Response, NextFunction } from 'express';
import { unidadeSaude } from '../../../entidades/unidadeSaude';
import { UnidadeSaudeRepositorio } from '../../../persistencia/repositorio/UnidadeSaudeRepositorio';


export async function inserirUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        console.log(req.body)
        const unidadeSaude = req.body as unidadeSaude;
        await UnidadeSaudeRepositorio.inserirUnidade(unidadeSaude);
        res.status(201).send('Cadastro realizado');
    } catch (e) {
        next(e);
    }
}

export async function getUnidades(req: Request, res: Response, next: NextFunction) {
    try {
        const unidadeSaudes = await UnidadeSaudeRepositorio.getUnidades();
        res.json(unidadeSaudes);
    } catch (e) {
        next(e);
    }
}

export async function removerUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const a = req.params.id;
        const unidadeSaudes = await UnidadeSaudeRepositorio.removerUnidade(a);
        res.json(unidadeSaudes);
    } catch (e) {
        next(e);
    }
}

export async function getunidadeSaudeById(req: Request, res: Response, next: NextFunction) {
    try {
        const idunidadeSaude = req.params.id;
        const unidadeSaude = await UnidadeSaudeRepositorio.getUnidadeById(idunidadeSaude);
        res.json(unidadeSaude);
    } catch (e) {
        next(e);
    }
}

export async function atualizarUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.body.id;
        const unidadeSaude = req.body.unidadeSaude;
        const unidadeSaudeAtt = await UnidadeSaudeRepositorio.atualizarUnidade(id, unidadeSaude);
        res.json(unidadeSaudeAtt);
    } catch (e) {
        next(e);
    }
}

export async function getTemposUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.body.id;
        const tempos = await UnidadeSaudeRepositorio.getTemposUnidade(id);
        res.json(tempos);
    } catch (e) {
        next(e);
    }
}




