import { atendimentoModel } from "../model/atendimentoModel";
import { atendimento } from "../../entidades/atendimento";

export class AtendimentoRepositorio{
    // lista todos os atendimentos
    static async getAtendimentos():Promise<atendimento[]>{
        return atendimentoModel.find().populate('paciente').exec();
    }
    
    // lista todos os atendimentos em uma determinada data
    static async getAtendimentosByData(data:Date):Promise<atendimento[]>{
        return atendimentoModel.find({'dataAtendimento':{
            $gte:data,
            $lt:new Date(data.getTime() + 24*60*60*1000)
        }}).populate('paciente').exec();
    }

    // lista todos os atendimentos de uma determinada unidade
    static async getAtendimentosByUnidade(idUnidade:number):Promise<atendimento[]>{
        return atendimentoModel.find().where('idUnidade').equals(idUnidade).populate('paciente').exec();
    }

    // busca atendimento por id
    static async getAtendimentoById(id:string):Promise<atendimento|null>{
        return atendimentoModel.findById(id).populate('paciente').exec();
    }

    static async inserirAtendimento(atendimento: atendimento){
        return atendimentoModel.create(atendimento);
    }

    static async atualizarAtendimento(id:string, atend:atendimento):Promise<atendimento>{
        const atendimento = await atendimentoModel.findById(id).exec();
        if(atendimento){
            atendimento.idAtendimento = atend.idAtendimento;
            atendimento.idUnidade = atend.idUnidade;
            atendimento.paciente = atend.paciente,
            atendimento.dataAtendimento = atend.dataAtendimento,
            atendimento.duracao = atend.duracao,
            atendimento.possibContagio = atend.possibContagio,
            atendimento.teste1 = atend.teste1,
            atendimento.teste2 = atend.teste2

            return atendimento.save();
        }else{
            throw new Error('id inexistente');
        }
    }

    static async removerAtendimento(id:string):Promise<atendimento>{
        const atendimento = await atendimentoModel.findById(id);
        if(atendimento){
            return atendimento.remove();
        }else{
            throw new Error('id inexistente')
        }
    }
}
