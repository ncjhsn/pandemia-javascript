import { paciente } from "../../entidades/paciente";
import { Document, Schema, Model, model } from "mongoose";

export interface PacienteDocument extends paciente, Document{}

const enderecoSchema = new Schema({
    rua:{type:String, required:true},
    numero:{type:Number, required:true},
    bairro:{type:String, required:true},
    cidade:{type:String, required:true}
});

const pacienteSchema = new Schema({
    nome:{type:String, required:true},
    cpf:{type:String, required: true, unique:true},
    numContato:{type:String},
    idade:{type:Number, required:true},
    sexo:{type:String, required:true},
    peso:{type:Number},
    altura:{type:Number},
    endereco:{type: enderecoSchema}
});

export const pacienteModel:Model<PacienteDocument> = model<PacienteDocument>('Paciente', pacienteSchema, 'pacientes');