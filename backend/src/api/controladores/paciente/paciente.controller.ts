import { Request, Response, NextFunction } from 'express';
import { paciente } from '../../../entidades/paciente';
import { PacienteRepositorio } from '../../../persistencia/repositorio/PacienteRepositorio';


export async function inserirPaciente(req: Request, res: Response, next: NextFunction) {
    try {
        console.log(req.body)
        const paciente = req.body as paciente;
        await PacienteRepositorio.inserirPaciente(paciente);
        res.status(201).send('Cadastro realizado');
    } catch (e) {
        next(e);
    }
}

export async function getpacientes(req: Request, res: Response, next: NextFunction) {
    try {
        const pacientes = await PacienteRepositorio.getPacientes();
        res.json(pacientes);
    } catch (e) {
        next(e);
    }
}

export async function deletarPaciente(req: Request, res: Response, next: NextFunction) {
    try {
        const a = req.params.id;
        const pacientes = await PacienteRepositorio.removerPaciente(a);
        res.json(pacientes);
    } catch (e) {
        next(e);
    }
}

export async function getpacienteById(req: Request, res: Response, next: NextFunction) {
    try {
        const idpaciente = req.params.id;
        const paciente = await PacienteRepositorio.getPacienteById(idpaciente);
        res.json(paciente);
    } catch (e) {
        next(e);
    }
}
export async function getPacienteByCpf(req: Request, res: Response, next: NextFunction) {
    try {
        const cpf = req.params.cpf;
        const paciente = await PacienteRepositorio.getPacienteByCpf(cpf);
        res.json(paciente);
    } catch (e) {
        next(e);
    }
}

export async function atualizarPaciente(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.body.id;
        const paciente = req.body.paciente;
        const pacienteAtt = await PacienteRepositorio.atualizarPaciente(id, paciente);
        res.json(pacienteAtt);
    } catch (e) {
        next(e);
    }
}





