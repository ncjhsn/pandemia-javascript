export interface paciente {
    nome: string,
    cpf: string,
    numContato?: string,
    idade: number,
    sexo: string,
    peso?: number,
    altura?: number,
    endereco?: {
        rua: string,
        numero: number,
        bairro: string,
        cidade: string
    }
}
