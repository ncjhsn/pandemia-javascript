import { unidadeSaude } from "../../entidades/unidadeSaude";
import { Document, Schema, Model, model } from "mongoose";

export interface UnidadeSaudeDocument extends unidadeSaude, Document{}

const enderecoSchema = new Schema({
    rua:{type: String, required:true},
    numero:{type:Number, required:true},
    bairro:{type:String, required:true},
    cidade:{type:String, required:true}
})

const unidadeSaudeSchema = new Schema({
    idUnidade:{type:Number},
    nome:{type:String, required:true},
    email:{type:String, required:true},
    endereco:{type:enderecoSchema}
});

export const unidadeSaudeModel:Model<UnidadeSaudeDocument> = model<UnidadeSaudeDocument>('UnidadeSaude', unidadeSaudeSchema, 'unidadesdesaude');