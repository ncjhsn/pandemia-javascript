import express, { Router } from 'express';
import bodyParser from 'body-parser';
import errorHandler from 'errorhandler';
import cors from 'cors';
import { routerPaciente } from './routes/paciente.routes';
import { routerUnidade } from './routes/unidade.routes';
import { routerAtendimento } from './routes/atendimento.routes';

const app = express();

app.set('port', process.env.PORT);
app.use(routerPaciente);
app.use(routerUnidade);
app.use(routerAtendimento);

if (process.env.NODE_ENV === 'development') { //SOMENTE para ambiente de desenvolvimento 
    app.use(errorHandler());
}

app.use(cors());
app.use(bodyParser.json());

export default app;
