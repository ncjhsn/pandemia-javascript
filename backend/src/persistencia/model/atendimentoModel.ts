import { atendimento } from "../../entidades/atendimento";
import { Document, Schema, SchemaTypes, model, Model } from "mongoose";
import { type } from "os";

export interface AtendimentoDocument extends atendimento, Document{}

const atendimentoSchema = new Schema({
    idAtendimento:{type:String},
    idUnidade:{type:Number},
    paciente: {type:SchemaTypes.ObjectId, ref:'Paciente', required:true},
    dataAtendimento:{type:Date, required:true}, 
    duracao:{type:Number, required:true},
    possibContagio:{type:Boolean, required:true},
    teste1:{type:Boolean, required:false},
    teste2:{type:Boolean, required:false}
});

export const atendimentoModel:Model<AtendimentoDocument> = model<AtendimentoDocument>('Atendimento', atendimentoSchema, 'atendimentos');